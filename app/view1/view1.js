'use strict';

var myApp = angular.module('myApp.view1', ['ngRoute', 'ngResource'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view1', {
    templateUrl: 'view1/view1.html',
    controller: 'View1Ctrl'
  });
}])

.controller('View1Ctrl', function View1Ctrl($scope, incomes, users) {
	$scope.incomes = incomes.query();
	users.query().$promise.then(function(result) {
		$scope.users = result;
		var usersByUrl = {};
		angular.forEach($scope.users, function(value, key) {
			var user = value;
			this[user.url] = user;
			console.log();
		}, usersByUrl);

		$scope.usersByUrl = usersByUrl;

	});

});

angular.
  module('myApp.view1').config(function($httpProvider, $resourceProvider){
$resourceProvider.defaults.stripTrailingSlashes = false;
$httpProvider.defaults.headers.common['Authorization'] = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJvcmlnX2lhdCI6MTQ2NTk0MTIyMywidXNlcm5hbWUiOiJqb2huZGl4b25AZXhhbXBsZS5jb20iLCJ1c2VyX2lkIjo1ODIsImxhc3RfcGFzc3dvcmRfY2hhbmdlIjoxNDY1OTM2NzE4LjI0MzUxNCwiZW1haWwiOiJqb2huZGl4b25AZXhhbXBsZS5jb20iLCJleHAiOjE0NjY1NDYwMjN9.EFCr2khLTP9Q8Wp0H3t0BkNhv-XMuOwz7q6Lcb8rXuc';
}).
  factory('incomes', ['$resource',
    function($resource) {
      return $resource('https://staging.qwil.co/platforms/2/income/', {}, {

      });
    }
  ]).factory('users', ['$resource', 
  	function($resource) {
  		return $resource('https://staging.qwil.co/platforms/2/contractors/', {}, {});
  	}
  ]);

// myApp.controller('View1Ctrl', function View1Ctrl($scope) {
//   $scope.incomes = [
// 		{
// 			email: 'bob@abc.com',
// 			amount: 100.00,
// 			notes: "Good work!"
// 		},

// 		{
// 			email: 'john@abc.com',
// 			amount: 200.00,
// 			notes: "Good work!"
// 		},

// 		{
// 			email: 'bill@abc.com',
// 			amount: 1000.00,
// 			notes: "Good job!"
// 		},
// 	];
// });